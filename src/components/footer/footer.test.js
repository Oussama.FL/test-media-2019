import React from 'react';
import { shallow } from 'enzyme';
import Footer from './footer.component';

describe('Footer Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Footer />);
    expect(component).toMatchSnapshot();
  });
});