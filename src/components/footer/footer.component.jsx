import React from 'react';

import './footer.component.scss';

const Footer = () => (
  <div className='footer'>
    <span className='footer__title'>
        REJOIGNEZ
      {' '}
      <b>EMIRATES | THE LIST</b>
    </span>
    <span className='footer__description'>
        VOL + HÔTEL NÉGOCIÉS JUSQU&#39;À - 70%
    </span>
    <hr className='footer__line' />
  </div>
);

export default Footer;
