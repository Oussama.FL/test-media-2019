import React from 'react';

import './header.component.scss';

const Header = () => (
  <div className='header'>
    <div className='header__title'>
        DÉCOUVREZ LES OFFRES DU MOMENT
    </div>
    <div className='header__description'>
        VOL + HÔTEL JUSQU&#39;À - 70%
    </div>
    <hr className='header__line' />
  </div>
);

export default Header;
