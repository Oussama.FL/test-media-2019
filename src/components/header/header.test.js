import React from 'react';
import { shallow } from 'enzyme';
import Header from './header.component';

describe('Header Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Header />);
    expect(component).toMatchSnapshot();
  });
});