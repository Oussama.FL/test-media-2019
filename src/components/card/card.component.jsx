import React from 'react';
import PropTypes from 'prop-types';

import Slider from '../slider/slider.component';

import './card.component.scss';


const Card = (props) => {
  const getTag = (classname) => {
    const tag = props.destination.tags.find((item) => item.classname === classname);
    return tag ? tag.label : '';
  };

  const getRating = () => {
    const rating = parseInt(props.destination.rating.replace('*', ''), 10);
    const ratingComponent = [];
    for (let i = 0; i < rating; i += 1) {
      ratingComponent.push(<i key={i} className='material-icons'>star</i>);
    }
    return ratingComponent;
  };

  return (
    <div className='card'>
      <div className='card__header'>
        <Slider images={props.images} />
        <div className='card__header__text'><span>{props.destination.upto}</span></div>
      </div>
      <div className='card__main'>
        <div className='card__main__content'>
          <div className='card__main__content__description'>
            <span className='card__main__content__description__country'>
              {props.destination.country}
              {' '}
              -
              {' '}
            </span>
            <span>{props.destination.place}</span>
            <br />
            <span className='card__main__content__description__label'>
              {props.destination.label}
              {' '}
            </span>
            <span className='card__main__content__description__rate'>
              {getRating()}
            </span>
          </div>
          <div className='card__main__content__extra'>
            <div className='card__main__content__extra__premium'>
              {getTag('premium')}
            </div>
            <div className='card__main__content__extra__option'>
              {getTag('option')}
            </div>
          </div>
        </div>
        <div className='card__main__action'>
          <button className='card__main__action__button'>
            <i className='material-icons'>arrow_forward_ios</i>
          </button>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  destination: PropTypes.object,
  images: PropTypes.arrayOf(PropTypes.string),
};

Card.defaultProps = {
  destination: {
    tags: [],
    rating: '0*',
  },
  images: [],
};

export default Card;
