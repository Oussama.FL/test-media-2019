import React from 'react';
import { shallow } from 'enzyme';
import Card from './card.component';

describe('Card Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Card />);
    expect(component).toMatchSnapshot();
  });
});