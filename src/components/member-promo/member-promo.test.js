import React from 'react';
import { shallow } from 'enzyme';
import MemberPromo from './member-promo.component';

describe('MemberPromo Component', () => {
  it('should render correctly', () => {
    const component = shallow(<MemberPromo />);
    expect(component).toMatchSnapshot();
  });
});