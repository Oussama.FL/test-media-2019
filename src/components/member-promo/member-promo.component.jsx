import React from 'react';

import './member-promo.component.scss';

const MemberPromo = () => (
  <div className='member-promo'>
    <div className='member-promo__title'>
      Déjà membre?
    </div>
    <div className='member-promo__description'>
        Votre code promo vous attend directement sur le site
      {' '}
      <a href='#'>en cliquant ici</a>
    </div>
  </div>
);

export default MemberPromo;
