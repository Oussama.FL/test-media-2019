import React from 'react';
import { shallow } from 'enzyme';
import Dropdown from './dropdown.component';

describe('Dropdown Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Dropdown />);
    expect(component).toMatchSnapshot();
  });
});