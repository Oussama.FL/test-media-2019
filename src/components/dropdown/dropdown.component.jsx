import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './dropdown.component.scss';

const Dropdown = (props) => {
  const [active, setActive] = useState(false);
  const [headerText, setHeaderText] = useState('DESTINATIONS');

  const select = (item) => {
    props.onSelect(item);
    setHeaderText(item === '' ? 'DESTINATIONS' : item);
    setActive(false);
  };

  return (
    <div className='dropdown'>
      <div className='dropdown__header'>
        <div className='dropdown__header__before'>
          <i className='material-icons'>tune</i>
        </div>
        <div className='dropdown__header__text'>
          <span>{headerText}</span>
        </div>
        <div className='dropdown__header__after' onClick={() => setActive(!active)}>
          <i className='material-icons'>keyboard_arrow_down</i>
        </div>
      </div>
      <div className={active ? 'dropdown__items' : 'dropdown__items--hide'}>
        <div
          className='dropdown__items__item'
          onClick={() => select('')}
        >
          <span>Afficher Tout</span>
        </div>
        {
          props.places.map((item) => (
            <div
              key={item}
              className='dropdown__items__item'
              onClick={() => select(item)}
            >
              <span>{item}</span>
            </div>
          ))
        }
      </div>
    </div>
  );
};

Dropdown.propTypes = {
  places: PropTypes.arrayOf(PropTypes.string),
  onSelect: PropTypes.func,
};

Dropdown.defaultProps = {
  places: [],
  onSelect: () => { },
};

export default Dropdown;
