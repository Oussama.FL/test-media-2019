import React from 'react';
import { shallow } from 'enzyme';
import OfferInfo from './offer-info.component';

describe('OfferInfo Component', () => {
  it('should render correctly', () => {
    const component = shallow(<OfferInfo />);
    expect(component).toMatchSnapshot();
  });
});