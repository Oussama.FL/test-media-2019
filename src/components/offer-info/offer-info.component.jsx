import React from 'react';

import './offer-info.component.scss';

const OfferInfo = () => (
  <div className='offer-info'>
    <div className='offer-info__title'>
        COMMENT BÉNÉFICIER DE L&#39;OFFRE ?
    </div>
    <div className='offer-info__description'>
        150€ DE RÉDUCTION* DÈS 1000€ D&#39;ACHAT
    </div>
    <hr className='offer-info__line' />
  </div>
);

export default OfferInfo;
