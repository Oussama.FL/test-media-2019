import React from 'react';
import { shallow } from 'enzyme';
import Stepper from './stepper.component';

describe('Stepper Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Stepper />);
    expect(component).toMatchSnapshot();
  });
});