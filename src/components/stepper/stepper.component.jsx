import React from 'react';

import './stepper.component.scss';


const Stepper = () => (
  <div className='stepper'>
    <div className='stepper__steps'>
      <div className='stepper__steps__step'>
        <div className='stepper__steps__step__number'>1</div>
        <div className='stepper__steps__step__content'>
          <span>REJOIGNEZ</span>
          {' '}
          <br />
          <span className='stepper__steps__step__content--red'>EMIRATES | THE LIST</span>
        </div>
        <div className='stepper__steps__step__line' />
      </div>

      <div className='stepper__steps__step'>
        <div className='stepper__steps__step__number'>2</div>
        <div className='stepper__steps__step__content'>
          <span>RECEVEZ PAR EMAIL</span>
          {' '}
          <br />
          <span className='stepper__steps__step__content--red'>VOTRE BON DE 150€ OFFERT</span>
          {' '}
          <br />
          <span>À UTILISER DÈS 1000€ D&#39;ACHAT</span>
        </div>
        <div className='stepper__steps__step__line' />
      </div>

      <div className='stepper__steps__step'>
        <div className='stepper__steps__step__number'>3</div>
        <div className='stepper__steps__step__content'>
          <span>RÉSERVEZ VOTRE SÉJOUR</span>
          {' '}
          <br />
          <span className='stepper__steps__step__content--red'>AVANT LE 30 AVRIL 2019</span>
        </div>
      </div>
    </div>
    <div className='stepper__action'>
      <button>JE M&#39;INSCRIS</button>
    </div>
  </div>
);

export default Stepper;
