import React from 'react';
import { shallow } from 'enzyme';
import Slider from './slider.component';

describe('Slider Component', () => {
  it('should render correctly', () => {
    const component = shallow(<Slider />);
    expect(component).toMatchSnapshot();
  });
});