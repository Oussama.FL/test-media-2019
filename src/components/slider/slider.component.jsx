import React, { useState } from 'react';
import PropTypes from 'prop-types';

import './slider.component.scss';


const Slider = (props) => {
  const [selected, setSelected] = useState(0);

  const next = () => {
    if (selected === props.images.length - 1) {
      setSelected(0);
    } else {
      setSelected(selected + 1);
    }
  };

  const previous = () => {
    if (selected === 0) {
      setSelected(props.images.length - 1);
    } else {
      setSelected(selected - 1);
    }
  };

  return (
    <div className='slider'>
      {
        props.images.map((img, index) => (
          <div
            key={img}
            className={selected === index ? 'slider__slide slider__slide--selected' : 'slider__slide'}
            style={{ backgroundImage: `url(${img})` }}
          >
            {
                props.images.length > 1
                && (
                  <>
                    <div
                      className='slider__slide__control slider__slide__control--next'
                      onClick={next}
                    >
                      <i className='material-icons'>navigate_next</i>
                    </div>
                    <div
                      className='slider__slide__control slider__slide__control--before'
                      onClick={previous}
                    >
                      <i className='material-icons'>navigate_before</i>
                    </div>
                  </>
                )
              }
          </div>
        ))
      }
    </div>
  );
};

Slider.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string),
};

Slider.defaultProps = {
  images: [],
};

export default Slider;
