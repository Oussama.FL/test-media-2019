import React from 'react';
import ReactDOM from 'react-dom';

import './style.scss';

import Root from './root/root.component';

ReactDOM.render(
  <Root />,
  document.getElementById('root'),
);
