const initialState = {};

export default function destinationsReducer(state = initialState, action) {
  switch (action.type) {
    case 'GET_DESTINATIONS':
      return { ...state, list: action.data };
    case 'GET_PLACES':
      return { ...state, places: action.data };
    case 'GET_DESTINATIONS_ERROR':
    case 'GET_PLACES_ERROR':
      return { ...state, error: action.error };
    default:
      return state;
  }
}
