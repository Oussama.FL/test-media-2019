import { combineReducers } from 'redux';
import destinationsReducer from './destinations.reducer';

const rootReducer = combineReducers({
  destinations: destinationsReducer,
});

export default rootReducer;
