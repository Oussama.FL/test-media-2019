import Destinations from '../services/destinations.service';

export function getDestinations() {
  return (dispatch) => {
    Destinations.get()
      .then((response) => dispatch({ type: 'GET_DESTINATIONS', data: response.data.destinations }))
      .catch((error) => dispatch({ type: 'GET_DESTINATIONS_ERROR', error }));
  };
}

export function getPlaces() {
  return (dispatch) => {
    Destinations.get()
      .then((response) => dispatch({
        type: 'GET_PLACES',
        data: response.data.destinations
          .map((destination) => destination.place)
          .filter((place, index, self) => self.indexOf(place) === index),
      }))
      .catch((error) => dispatch({ type: 'GET_PLACES_ERROR', error }));
  };
}
