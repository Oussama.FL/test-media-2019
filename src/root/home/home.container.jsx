import { connect } from 'react-redux';

import { getDestinations, getPlaces } from '../../common/actions/destinations.action';

import Home from './home.component';

const mapStateToProps = (state) => ({
  destinations: state.destinations.list,
  places: state.destinations.places,
  error: state.destinations.error,
});

const mapDispatchToProps = (dispatch) => ({
  getDestinations: () => {
    dispatch(getDestinations());
  },
  getPlaces: () => {
    dispatch(getPlaces());
  },
});

const HomeContainer = connect(mapStateToProps, mapDispatchToProps)(Home);

export default HomeContainer;
