import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';


import Header from '../../components/header/header.component';
import Dropdown from '../../components/dropdown/dropdown.component';
import Card from '../../components/card/card.component';
import OfferInfo from '../../components/offer-info/offer-info.component';
import MemberPromo from '../../components/member-promo/member-promo.component';
import Stepper from '../../components/stepper/stepper.component';
import Footer from '../../components/footer/footer.component';

import './home.component.scss';

const Home = (props) => {
  const [destinations, setDestination] = useState([]);

  useEffect(() => {
    props.getDestinations();
    props.getPlaces();
  }, []);

  useEffect(() => {
    setDestination(props.destinations);
  }, [props.destinations]);

  const getImage = (label, index) => {
    switch (label) {
      case 'Laguna Beach Hotel & Spa':
      case 'Impiana Resort Samui':
      case 'Grand Arc Hanzomon':
        if (index === 0) {
          return [
            `assets/images/${index + 1}.png`,
            `assets/images/${index + 2}.png`,
            `assets/images/${index + 3}.png`,
          ];
        }
        return [
          `assets/images/${index + 1}.png`,
          `assets/images/${index}.png`,
          `assets/images/${index + 2}.png`,
        ];


      default:
        return [`assets/images/${index + 1}.png`];
    }
  };

  const filter = (place) => {
    if (place && place !== '') {
      setDestination(props.destinations.filter((item) => item.place === place));
    } else {
      setDestination(props.destinations);
    }
  };

  return (
    <div className='home'>
      <Header />

      <Dropdown
        places={props.places}
        onSelect={filter}
      />

      <div className='home__cards'>
        {
          destinations.map((destination, index) => (
            <div key={destination.label}>
              <Card destination={destination} images={getImage(destination.label, index)} />
            </div>
          ))
        }
      </div>

      <OfferInfo />

      <MemberPromo />

      <Stepper />

      <Footer />
    </div>
  );
};

Home.propTypes = {
  destinations: PropTypes.array,
  places: PropTypes.array,
  getDestinations: PropTypes.func,
  getPlaces: PropTypes.func,
};

Home.defaultProps = {
  destinations: [],
  places: [],
  getDestinations: () => { },
  getPlaces: () => { },
};

export default Home;
