import React from 'react';
import { Provider } from 'react-redux';
import configureStore from '../common/stores';

import HomeContainer from './home/home.container';

const Root = () => (
  <Provider store={configureStore({})}>
    <HomeContainer />
  </Provider>
);

export default Root;
