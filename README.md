# Test Développement Front Media 2019

## Mise en place du projet

Pour exécuter ce projet, assurez-vous d’avoir installé Node.JS. J’ai utilisé la version 10, mais la version 8 devrait convenir. Les instructions sont les suivantes:

* **_npm install :_** pour installer les dépendances.
* **_npm start :_** pour démarrer le serveur dev (localhost: 8080).
* **_npm run build :_** pour construire une version dev du projet.
* **_npm run build:prod :_** pour construire une version prod du projet (version optimisé et minifié).
* **_npm run lint :_** pour vérifier les fichiers js et jsx avec eslint.
* **_npm run lint:fix :_** pour vérifier et corriger les fichiers js et jsx en utilisant eslint.
* **_npm run test :_** pour tester les composants à l'aide des snapshots.
* **_npm run test:update :_** pou mettre à jour des snapshots des composants.

Comme mentionné précédemment, Node.JS 10 a été utilisé lors du développement du projet et Google Chrome 76 et Firefox 69 lors de son test. L'émulateur Glaxy S5 de Google Chrome (360 X 640) a été utilisé pour tester la version mobile.
